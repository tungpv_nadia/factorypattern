<?php
/**
 * 
 */
namespace Factory;
class Car
{
	
	private $make;
	private $model;

	public function __construct($make, $model){
		$this->make = $make;
		$this->model = $model;
	}
	public function getAll(){
		$result = array(
			'make' => $this->make,
			'model' => $this->model 
			);
		return $result;
	}
} 
?>