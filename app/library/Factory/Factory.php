<?php

 /**
 * Factory
 */
 namespace Factory;
 use Factory\Car;
 class Factory extends Car
 {
 	
 	public static function create($make, $model)
 	{
 		return new Factory($make, $model);
 	}
 }
 ?>